package com.example.demogateway.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

/**
 * 响应
 */
public final class RestResponse<T> {

	private static final Logger logger = LoggerFactory.getLogger(RestResponse.class);

	/**
	 * 最大异常信息
	 */
	private static final int ERROR_MSG_LENGTH = 200;

	/**
	 * 标明返回结果是哪个应用返回的
	 */
	private  String from = "gateway";

	/**
	 * 返回码
	 */
	private int code = 400;



	/**
	 * 提示信息
	 */
	private String msg = "";

	/**
	 * 业务数据
	 */
	private Object data = null;

	/**
	 * 异常结果code
	 */
	public static final int ERROR_CODE = 1;
	
	private RestResponse() {

	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public static RestResponse success(Object data) {
		RestResponse resp = new RestResponse();
		resp.setCode(0);

		resp.setMsg("成功");
		resp.setData(data);
		return resp;
	}

	public static RestResponse success(Object data,String msg) {
		RestResponse resp = new RestResponse();
		resp.setCode(0);

		resp.setMsg(msg);
		resp.setData(data);
		return resp;
	}



	public static RestResponse fail(int errorCode, String errorMsg) {
		RestResponse resp = new RestResponse();
		resp.setCode(errorCode);

		resp.setMsg(errorMsg);
		return resp;
	}
	public static RestResponse build(int code, String msg,Object data) {
		RestResponse resp = new RestResponse();
		resp.setCode(code);

		resp.setMsg(msg);
		resp.setData(data);
		return resp;
	}
	public static RestResponse build(int code, String msg) {

		return build(  code,   msg,null);
	}




}