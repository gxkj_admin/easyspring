package com.example.demogateway.error;

import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.DefaultErrorWebExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.server.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;

public class JsonExceptionHandler extends DefaultErrorWebExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(JsonExceptionHandler.class);

    public JsonExceptionHandler(ErrorAttributes errorAttributes, ResourceProperties resourceProperties,
                                ErrorProperties errorProperties, ApplicationContext applicationContext) {
        super(errorAttributes, resourceProperties, errorProperties, applicationContext);
    }

    /**
     * 获取异常属性
     */
    @Override
    protected Map<String, Object> getErrorAttributes(ServerRequest request, boolean includeStackTrace) {
        int code = HttpStatus.INTERNAL_SERVER_ERROR.value();
        Throwable error = super.getError(request);
        if (error instanceof org.springframework.cloud.gateway.support.NotFoundException) {
            code = HttpStatus.NOT_FOUND.value();
        }else if(error instanceof  org.springframework.web.server.ResponseStatusException){
            ResponseStatusException ex  = (ResponseStatusException)error;
            code =  ex.getStatus().value();
        }
        Map<String, Object>  rep = new HashMap<>();
        rep.put("from","gateway");
        if(code == 404){
            rep.put("code",404);
            rep.put("msg",request.uri()+":"+error.getMessage());
            return rep;
        }else if(code == 500){

            rep.put("code",500);
            rep.put("msg",request.uri()+":"+error.getMessage());
            return rep;
        }else{

            rep.put("code",code);
            rep.put("msg",request.uri()+":"+error.getMessage());

        }
        return rep;
//        return response(code, this.buildMessage(code,request, error));
    }

    /**
     * 指定响应处理方法为JSON处理的方法
     * @param errorAttributes
     */
    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(ErrorAttributes errorAttributes) {
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
    }


    /**
     * 根据code获取对应的HttpStatus
     * @param errorAttributes
     */
    @Override
    protected HttpStatus getHttpStatus(Map<String, Object> errorAttributes) {
        int statusCode = (int) errorAttributes.get("code");
        return HttpStatus.valueOf(statusCode);
    }

    /**
     * 构建异常信息
     * @param request
     * @param error
     * @return
     */
    private String buildMessage(int code ,ServerRequest request, Throwable error) {


        if (error instanceof org.springframework.cloud.gateway.support.NotFoundException) {
//            code = HttpStatus.NOT_FOUND.value();
        }else if(error instanceof  org.springframework.web.server.ResponseStatusException){
            ResponseStatusException ex  = (ResponseStatusException)error;
//            code =  ex.getStatus().value();
        }

        StringBuilder message = new StringBuilder("Failed to handle request [");
        message.append(request.methodName());
        message.append(" ");
        message.append(request.uri());
//        message.append("]");
//        if (ex != null) {
//            message.append(": ");
//            message.append(ex.getMessage());
//        }
        return message.toString();
    }

    /**
     * 构建返回的JSON数据格式
     * @param status        状态码
     * @param errorMessage  异常信息
     * @return
     */
    public static Map<String, Object> response(int status, String errorMessage) {
        logger.info("异常了");
        Map<String, Object> map = new HashMap<>();
        map.put("code", status);
        map.put("message", errorMessage);
        map.put("data", null);
        logger.error(map.toString());
        return map;
    }
}
