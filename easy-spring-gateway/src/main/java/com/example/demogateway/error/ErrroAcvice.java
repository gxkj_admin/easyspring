package com.example.demogateway.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrroAcvice {

    private static final Logger logger = LoggerFactory.getLogger(ErrroAcvice.class);

    /**
     * 全局捕获异常的切面类
     * @param e 异常类(这个要和你当前捕获的异常类是同一个)
     */
    @ExceptionHandler(Exception.class) //也可以只对一个类进行捕获
    public void errorHandler(WebFilterExchange webFilterExchange, Exception e){


        logger.error("异常",e);
        // 根据这个字符串来判断做



    }
}
