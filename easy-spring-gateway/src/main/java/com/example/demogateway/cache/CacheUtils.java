package com.example.demogateway.cache;

import com.alibaba.fastjson.JSON;
import com.example.common.entitys.AuthUserDetails;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class CacheUtils {
    private  static Map<String, String>  token2UserCache = new HashMap<>();

    //将来使用其他缓存可以将此类进行修改
    public static void put(String key,String value){
        token2UserCache.put(key,value);
    }
    //将来使用其他缓存可以将此类进行修改
    public static String getString(String key){
        return token2UserCache.get(key);
    }
    //将来使用其他缓存可以将此类进行修改
    public static AuthUserDetails getAuthUserDetails(String key){
        String v=  token2UserCache.get(key);
        if(StringUtils.isEmpty(v)){
            return null;
        }else {
            return JSON.parseObject(v,AuthUserDetails.class);
        }
    }

    public static void remove(String key){
        token2UserCache.remove(key);
    }


}
