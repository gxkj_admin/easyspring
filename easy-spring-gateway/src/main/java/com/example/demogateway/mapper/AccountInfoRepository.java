package com.example.demogateway.mapper;

import reactor.core.publisher.Mono;

public interface AccountInfoRepository {
    Mono<Object> findByUsername(String username);
}
