package com.example.config;

import com.example.utils.RestResponse;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class MyErrorAttribute extends DefaultErrorAttributes {

    @Override
    public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
        Map<String, Object> map =  new HashMap<>() ;
//        super.getErrorAttributes(webRequest, includeStackTrace);
        Integer status = (Integer)webRequest.getAttribute("javax.servlet.error.status_code",0);
        String path = (String) webRequest.getAttribute("javax.servlet.error.request_uri",0);
        map.put("code",status);
        map.put("timestamp",new Date());
        map.put("from", RestResponse.from);
        map.put("msg","发生错误,请求路径为："+path+",错误编码为："+status);
        return map;
    }
}
