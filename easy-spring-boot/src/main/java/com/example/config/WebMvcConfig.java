package com.example.config;

import com.example.filters.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfig   implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler(new String[]{"/js/*","/js/**"}).addResourceLocations(new String[]{"classpath:/content/js/"});

        registry.addResourceHandler(new String[]{"/statics/**"}).addResourceLocations(new String[]{"classpath:/statics/"});

    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //1.加入的顺序就是拦截器执行的顺序，
        //2.按顺序执行所有拦截器的preHandle
        //3.所有的preHandle 执行完再执行全部postHandle 最后是postHandle

//        registry.addInterceptor(new PerfInterceptor()).addPathPatterns("/sys/*/**","/admin/*/**");
//        registry.addInterceptor(new HttpParamVerifyInterceptor()).addPathPatterns("/sys/*/**","/admin/*/**");
        registry.addInterceptor(new TraceIdInterceptor()).addPathPatterns("/*/**");
        registry.addInterceptor(new PerfInterceptor()).addPathPatterns("/*/**");
        registry.addInterceptor(new UserInterceptor()).addPathPatterns("/*/**");
        registry.addInterceptor(new ShiroInterceptor()).addPathPatterns("/*/**");
        registry.addInterceptor(new HttpParamVerifyInterceptor()).addPathPatterns("/*/**");


    }

}
