package com.example.threadloacls;

import com.example.common.entitys.AuthUserDetails;

public class AuthUserDetailsThreadlocal {

    private static ThreadLocal<AuthUserDetails> userHolder = new ThreadLocal<AuthUserDetails>();

    public static AuthUserDetails getUserSession(){
        AuthUserDetails user = userHolder.get();

        return user;
    }
    public static void setUserSession(AuthUserDetails user){
        userHolder.set(user);
    }

    public static void removeUser(){
        userHolder.remove();
    }

}
