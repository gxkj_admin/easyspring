//package com.example.filters;
//
//
//import com.example.utils.TraceIdUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.slf4j.MDC;
//import org.springframework.core.annotation.Order;
//import org.springframework.util.StringUtils;
//
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import java.io.IOException;
//
///**
// * order值越小，Filter越早经过
// */
//@WebFilter(urlPatterns = "/*", filterName = "traceIdFilter")
//@Order(1)
//public class TraceIdFilter implements Filter {
//
//    private static final Logger logger = LoggerFactory.getLogger(TraceIdFilter.class);
//    public static final String traceIdTag = "traceId";
//
//
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//
//    }
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        logger.info("执行TraceIdFilter");
//        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
//        //先从param里取，没有的话从header里取，还没有的话再创建
//        String traceId = request.getParameter(traceIdTag);
//        if (!StringUtils.hasLength(traceId)){
//            traceId = httpServletRequest.getHeader(traceIdTag);
//        }
//        if (!StringUtils.hasLength(traceId)){
//            traceId = TraceIdUtils.getTraceId();
//        }
//
//        MDC.put(traceIdTag, traceId);
//
//        try{
//            chain.doFilter(request, response);
//
//        }catch (Exception e){
//
//            throw  e;
//        }finally {
//            MDC.remove(traceIdTag);
//        }
//    }
//
//    @Override
//    public void destroy() {
//
//    }
//}