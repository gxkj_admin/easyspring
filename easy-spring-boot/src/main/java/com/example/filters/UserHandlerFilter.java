//package com.example.filters;
//
//import com.alibaba.fastjson.JSON;
//import com.example.common.entitys.AuthUserDetails;
//import com.example.threadloacls.AuthUserDetailsThreadlocal;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.slf4j.MDC;
//import org.springframework.core.annotation.Order;
//import org.springframework.http.HttpHeaders;
//import org.springframework.util.StringUtils;
//
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import java.io.IOException;
//
//@WebFilter(urlPatterns = "/*", filterName = "userHandlerInterceptor")
//@Order(2)
//public class UserHandlerFilter implements Filter {
//
//    private static final Logger logger = LoggerFactory.getLogger(UserHandlerFilter.class);
//
//
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        logger.info("执行UserHandlerInterceptor");
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//
//        AuthUserDetails userDto =  null;
//        String url = request.getRequestURI();
//
//        String  currentuser = request.getHeader("currentuser");
//        String  token = request.getHeader(HttpHeaders.AUTHORIZATION);
//        logger.info("访问资源，url="+url+",currentuser="+currentuser+",token="+token);
//        MDC.put("currentuser",currentuser);
//        MDC.put("token",token);
//        if(StringUtils.hasLength(currentuser)){
//            userDto = JSON.parseObject(currentuser,AuthUserDetails.class);
//        }
//        if(userDto == null){
//            AuthUserDetailsThreadlocal.setUserSession(null);
//        }else{
//            AuthUserDetailsThreadlocal.setUserSession(userDto);
//
//        }
//
//        try{
//            filterChain.doFilter(request, servletResponse);
//
//        }finally {
////            if(AuthUserDetailsThreadlocal.getUserSession() != null){
////                AuthUserDetailsThreadlocal.removeUser();
////            }else{
////                AuthUserDetailsThreadlocal.removeUser();
////            }
////
////            MDC.remove("currentuser");
////            MDC.remove("token");
//        }
//    }
//
//
//}