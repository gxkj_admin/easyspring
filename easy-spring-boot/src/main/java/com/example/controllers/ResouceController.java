package com.example.controllers;

import com.example.utils.RestResponse;
import com.example.validator.Verify;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class ResouceController {

    @RequestMapping("/resouce/list")
    @RequiresPermissions("resouce:list")
    public RestResponse list(
                             HttpServletResponse response, HttpServletRequest request,
                             @Verify(param = "page",  rule = "required|min(1)") int page,
                             @Verify(param = "limit",  rule = "required|max(100)") int limit) {
        //查询列表数据
        RestResponse result = RestResponse.build(0,"成功获取到需要授权访问的资源");
        return result;
    }

    @RequestMapping("/resouce/info")
    @RequiresPermissions("resouce:info")
    public RestResponse info(
            HttpServletResponse response, HttpServletRequest request) {
        //查询列表数据
        RestResponse result = RestResponse.build(0,"成功获取到需要授权访问的资源");
        return result;
    }

}
