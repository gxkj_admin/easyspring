package com.example.controllers;

import com.example.utils.RestResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class CommonController {

    @RequestMapping("/common/list")

    public RestResponse list(
                             HttpServletResponse response, HttpServletRequest request) {
        //查询列表数据
        RestResponse result = RestResponse.build(0,"成功获取到通用的资源");
        return result;
    }

}
