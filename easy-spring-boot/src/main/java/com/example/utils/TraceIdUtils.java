package com.example.utils;

import org.slf4j.MDC;
import org.springframework.util.StringUtils;

import java.util.UUID;

public class TraceIdUtils {
    public  static String getTraceId(){
        String   traceId = MDC.get("traceId");
        if(StringUtils.hasLength(traceId)){
            return traceId;
        }else{
            traceId = UUID.randomUUID().toString().replace("-", "");
            return traceId;
        }
    }
}
