package com.example.utils;


import org.springframework.util.StringUtils;

public final class IdNumberUtil {
    /**
     * 身份证号码是否合法
     **/
    public static boolean check(String no) {
        // 1-17位相乘因子数组
        int[] factor = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
        // 18位随机码数组
        char[] random = "10X98765432".toCharArray();
        // 计算1-17位与相应因子乘积之和
        int total = 0;
        for (int i = 0; i < 17; i++) {
            total += Character.getNumericValue(no.charAt(i)) * factor[i];
        }
        // 判断随机码是否相等
        return random[total % 11] == no.charAt(17);
    }

    public static String hiddenIdCard(String idNumber, Integer prefix, Integer suffix) {
        if (!StringUtils.hasLength(idNumber)) {
            return null;
        }
        if (null == prefix || prefix <= 0) {
            return "隐藏前缀不能为null";
        }
        if (null == suffix || suffix <= 0) {
            return "隐藏的后缀不能为null";
        }
        return idNumber.replaceAll("(?<=\\w{" + prefix + "})\\w(?=\\w{" + suffix + "})", "*");
    }

    public static String hiddenIdCard(String idNumber) {
        if (!StringUtils.hasLength(idNumber)) {
            return null;
        }
        return idNumber.replaceAll("(?<=\\w{3})\\w(?=\\w{2})", "*");
    }

}