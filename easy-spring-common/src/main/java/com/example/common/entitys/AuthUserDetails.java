package com.example.common.entitys;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthUserDetails implements Serializable {
    private static final long serialVersionUID = 8241970228716425282L;
    private String username;

    private String password;

    private Integer status;

    private Map<String,String> permsMap = new HashMap<String,String>();

    public void setPermsList(List<String> permsList) {
        this.permsList = permsList;
        if(permsList != null){
            for(String key :permsList) {
                permsMap.put(key,key);
            }
        }

    }

    /**
     * 授权访问的资源列表
     */
    private List<String> permsList;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getPermsList() {
        return permsList;
    }



    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Map<String, String> getPermsMap() {
        return permsMap;
    }

    public void setPermsMap(Map<String, String> permsMap) {
        this.permsMap = permsMap;
    }
}
