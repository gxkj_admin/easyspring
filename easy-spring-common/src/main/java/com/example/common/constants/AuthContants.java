package com.example.common.constants;

public class AuthContants {

    public static final String HEADER_CURRENTUSER_TAG = "currentuser";
    public static final String AUTHORIZATION = "Authorization";

    public static final int SUCCESS_CODE = 0;
    public static final String LOGOUT_SUCCESS_MSG = "退出成功";

    public static final int AUTHORIZED_FAILURE_CODE = 400;
    public static final String AUTHORIZED_FAILURE_MSG = "授权失败";
    public static final int COMMON_AUTHORIZED_FAILURE_CODE = 401;
    public static final int NO_ENOUGH_AUTHORIZED_FAILURE_CODE = 402; //权限不足，请管理员授权
    public static final String COMMON_AUTHORIZED_FAILURE_MSG = "访问未授权资源，请联系管理员";

    public static final String COMMON_NOLOGIN_MSG = "您还没有登录，请先登录";


    public static final int TIMEOUT_FAILURE_CODE = 400;
}
