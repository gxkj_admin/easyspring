# easyspring

#### 介绍
使用springgateway整合springboot

#### 软件架构
-  **easy-spring-common**  公用的类在这里，包括登录用户和授权错误异常码
-  **easy-spring-gateway**  网关项目，默认端口80
-  **easy-spring-boot**     后端服务项目，默认端口81

#### 安装教程

1.  首先编译easy-spring-common。执行mvn:install就可以
2.  easy-spring-gateway和easy-spring-boot启动不分前后


#### 使用说明

#### 1.  登录验证
---

    1.1  url：http://localhost/auth/login
         参数：  
            username：
                     admin:正常登陆
                     test:返回状态错误，例如锁定了
                     其他：登录失败 
            password：1233456

#### 2.  退出
--- 
    2.1  url：http://localhost/auth/logout
          参数： 在header里放置登录时返回的token的值，header的key为：Authorization


#### 3.  访问不需要登录就可以访问的资源。如果登录了会在后面的服务中获取登录用户身份。否则获取不到。
---
     3.1  url：http://localhost/noneedlogin
       参数选填： 在header里放置登录时返回的token的值，header的key为：Authorization

#### 4.  访问需要登录就才可以访问的资源。否则提示需要授权。
---
     4.1  url：http://localhost/needLogin 或者 http://localhost/httpbin/hello
       参数选填： 在header里放置登录时返回的token的值，header的key为：Authorization

 

#### 5.  访问需要登录，并且需要授权才能访问的资源。未授权则提示需要授权。
---
     5.1  url：http://localhost/resouce/list?page=1&limit=1
       参数选填： 在header里放置登录时返回的token的值，header的key为：Authorization
       开发时需要注意：使用标签 @RequiresPermissions("resouce:list")，多个权限可以使用逗号分隔。

#### 6.  访问需要登录，并且需要授权才能访问的资源。未授权则提示需要授权。
---
     6.1  url：http://localhost/resouce/info
       参数选填： 在header里放置登录时返回的token的值，header的key为：Authorization.
        注意：这里没有被授权，所以出现拦截信息是对的。
       开发时需要注意：使用标签 @RequiresPermissions("resouce:list")，多个权限可以使用逗号分隔。

#### 7.  断路器验证
---
     7.1  
        7.1.1 url：http://localhost/timeout?timeout=3000
        7.1.2 参数选填： 在header里放置登录时返回的token的值，header的key为：Authorization.需要登录。参数timeout的值为响应的时间.
        7.1.3 超时返回到/hystrixfallback.
        7.1.4 跳转后traceId保持一致
#### 8.  网关层404等错误
---
     8.1  
        8.1.1 url：http://localhost/gateway404
        8.1.2 参数选填： 在header里放置登录时返回的token的值，header的key为：Authorization.需要登录。参数timeout的值为响应的时间.
        8.1.3 返回错误里有from:gateway

#### 9.  应用层404等错误
---
     9.1  
        9.1.1 url：http://localhost/app404
        9.1.2 参数选填： 在header里放置登录时返回的token的值，header的key为：Authorization.需要登录。参数timeout的值为响应的时间.
        9.1.3 返回错误里有from:app
    
#### 二次开发需要注意的事项

#### 1.  网关配置
---
1.1 在DemogatewayApplication.java类的customRouteLocator方法里自定义自己的网关；

1.2 例子中做了predicate判断是否适配的例子；

1.3 例子中做了needLoginGateWayFilterFactory来做需要登录才能继续访问的过滤器；

1.4 例子中做了needLoginGateWayFilterFactory来做不需要登录就能继续访问的过滤器；

1.5 SecurityConfig里做了security相关的配置；

1.6 用户登录认证在SecurityUserDetailsService里的findByUsername里进行认证。可二次发开改成从数据库查询；

1.7 认证后验证在AuthenticationSuccessHandler方法里验证；

1.8 断路器时间限制，参考application.yml里的配置；

1.9 com.netflix.hystrix.AbstractCommand 是从源文件中拷贝过来的，增加了MDC.setContextMap相关代码，目的是为了完成断路器中的MDC起作用；

1.10 设置最大上传大小，参考application.yml里的配置spring.servlet.multipart.max-file-size和spring.servlet.multipart.max-request-size；

1.11 缓存放到CacheUtils里，存储的是用户token信息；

1.12 如果需要补充登录用户的属性问题，在AuthenticationSuccessHandler的buildUser()方法里完成。


#### 参考文档

1.  流量控制，使用的是Sentinel，参考的是[官方springboot项目导入Sentinel的说明](https://github.com/alibaba/spring-cloud-alibaba/wiki/Sentinel)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
